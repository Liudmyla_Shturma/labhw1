const fs = require('fs');
const express = require('express');
const app = express();
app.use(express.json());

const basePath = './data/';

const getAllFiles = (req, res) => {

    const files = fs.readdirSync(basePath);

    res.status(200).json({
        message: 'Success',
        files: files
    });
};

const getFile = (req, res) => {
    console.log(req);

    const filename = req.params.filename;
    const filePath = basePath + '/' + filename;

    fs.open( filePath, 'r', (err, fd) => {
    if (err) {
      if (err.code === 'ENOENT') {
          return res.status(400).json({
              message: 'No file with '+ filename + ' filename found'
          })
      }
      throw err;
    };

    const { birthtime } = fs.statSync(filePath);

    let extension = '';
    if (filename.includes('.')) {
      extension = filename.split('.').pop();
    };

    const content = fs.readFileSync(fd,'utf8');
    
    res.status(200).json({
        message: 'success',     
        filename: filename,
	content: content,
	extension: extension,
	uploadedDate: birthtime
    });

  
});

};

const createFile = (req, res) => {   
    console.log(req);

    const params = req.body;
    const filename = params.filename;
    const content = params.content;

    
    if (!filename) {
       return res.status(400).json({
      	 message: "Please specify 'filename' parameter"
       })
    }
    
    if (!content) {
       return res.status(400).json({
      	 message: "Please specify 'content' parameter"
       })
    }

    const filePath = basePath + '/' + filename;

    fs.open(filePath, 'wx', (err, fd) => {
	  if (err) {
	        return res.status(500).json({
        	 message: 'Server error'
	        })
	     };

        fs.writeFileSync(fd,content);

        res.status(200).json({
        message: 'File created successfully'
       });

    });

};


if (!fs.existsSync(basePath)) {
    fs.mkdirSync(basePath);
}  

app
.route('/api/files')
.get(getAllFiles)
.post(createFile);

app
.route('/api/files/:filename')
.get(getFile)

const port = 8080;
app.listen(port, () => {
    console.log(`App running on port ${port}`)
})